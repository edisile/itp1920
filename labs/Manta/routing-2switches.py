#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf, Link
from subprocess import call

def myNetwork():

    net = Mininet( topo=None,
                   build=False )

    info( '*** Adding controller\n' )
    info( '*** Add switches\n')
    s1 = net.addHost('s1', cls=Node, ip=None)
    s2 = net.addHost('s2', cls=Node, ip=None)

    info( '*** Add hosts\n')
    h1 = net.addHost('h1', cls=Host, ip=None, mac='00:00:10:00:00:01')
    h2 = net.addHost('h2', cls=Host, ip=None, mac='00:00:10:00:00:02')
    h3 = net.addHost('h3', cls=Host, ip=None, mac='00:00:10:00:00:03')
    h4 = net.addHost('h4', cls=Host, ip=None, mac='00:00:10:00:00:04')
    h5 = net.addHost('h5', cls=Host, ip=None, mac='00:00:10:00:00:05')

   
    info( '*** Adding links\n')
    Link(s1, h1, intfName1='s1-eth1')
    Link(s1, h2, intfName1='s1-eth2')
    Link(s1, h3, intfName1='s1-eth3')

    Link(s2, h4, intfName1='s2-eth4')
    Link(s2, h5, intfName1='s2-eth5')

    Link(s1, s2, intfName1='s1-s2-eth', intfName2='s2-s1-eth')

    info( '*** Starting network\n')
    net.build()

    info( '*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info( '*** Starting switches\n')
    s1.intf( 's1-eth1' ).config(mac='00:00:10:00:01:01')
    s1.intf( 's1-eth2' ).config(mac='00:00:10:00:01:02')
    s1.intf( 's1-eth3' ).config(mac='00:00:10:00:01:03')

    s2.intf( 's2-eth4' ).config(mac='00:00:10:00:01:04')
    s2.intf( 's2-eth5' ).config(mac='00:00:10:00:01:05')

    info( '*** Turn off IPv6\n')
    s1.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    s1.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    s2.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    s2.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h1.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h1.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h2.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h2.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h3.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h3.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h4.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h4.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h5.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h5.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')


    info( '*** Starting bridges\n')
    s1.cmd('ip link add br0 type bridge')
    s1.cmd('brctl addbr br0')
    s1.cmd('brctl addif br0 s1-eth1')
    s1.cmd('brctl addif br0 s1-eth2')
    s1.cmd('brctl addif br0 s1-eth3')
    s1.cmd('brctl addif br0 s1-s2-eth')
    s1.cmd('ip link set br0 up')

    s2.cmd('ip link add br0 type bridge')
    s2.cmd('brctl addbr br0')
    s2.cmd('brctl addif br0 s2-s1-eth') # connection to s1
    s2.cmd('brctl addif br0 s2-eth4')
    s2.cmd('brctl addif br0 s2-eth5')
    s2.cmd('ip link set br0 up')



    info( '*** Setting IP\n')
    h1.cmd('ip a add 10.0.0.100/24 dev h1-eth0')
    h2.cmd('ip a add 10.0.0.101/24 dev h2-eth0')
    h3.cmd('ip a add 10.0.0.102/24 dev h3-eth0')

    h4.cmd('ip a add 10.0.0.103/24 dev h4-eth0')
    h5.cmd('ip a add 10.0.0.104/24 dev h5-eth0')
            

    info( '*** Post configure switches and hosts\n')

    CLI(net)
    net.stop()

if __name__ == '__main__':
    setLogLevel( 'info' )
    myNetwork()

